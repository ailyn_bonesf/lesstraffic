package com.lesstraffic.geolocationservice.listeners;

import com.lesstraffic.geolocationservice.model.Geolocation;
import com.lesstraffic.geolocationservice.services.GeolocationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@RefreshScope
public class GeolocationListener {
	private final static Logger LOGGER = LoggerFactory.getLogger(GeolocationListener.class);
	
	@Autowired private GeolocationService geolocationService;
	
	@KafkaListener(topics = "${lesstraffic.geolocation.topic.insert-node}")
	public void insertNode(Geolocation geolocation){
		LOGGER.debug("Leyendo de cola la geolocalizacion " + geolocation.toString());
		
		geolocationService.insertNode(geolocation);
	}
}
